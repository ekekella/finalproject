<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>My Book Tracker Database</title>
    <link rel="stylesheet" href="main.css">
</head>

<body>
    <header>
        <div class="container">
            <img src="images/mainPic.jpg" alt="Girl Reading in Library" style="width:100%;">
            <div class="topLeft"><h3 class="hT">Esther's Database</h3></div>
            <div class="centered">
                <p class="hT">Organization</p>
                <h1 class="hT">Begins Here With You</h1>
            </div>
        </div>
    </header>

    <main>
        <form action="Servlet" method="post">
            <div class="intro">
                <div class="begin">
                    <h2>Are you and avid reader?  Then this is the book database for you</h2>
                    <p>This is the perfect place to upload all the books you own.  This database includes a place for the Book Cover Image, Title, Author, Publisher, Genre, Number of Pages, your own rating of the book and a description.</p>
                </div>
            </div>
            <div class="wrapper">
                <div class="box1">
                    <img src="images/sorting.jpg" alt="Library">
                </div>
                <div class="box2" id="word4">
                    <h2>My Library of Books</h2>
                    <p>Sift through your collection of books here.</p>
                    <input type="submit" name="view" value="View Books" class="bSub">
                </div>
            </div>
            <div class="wrapper">
                <div class="box1" id="word6">
                    <h2>Add to My Library</h2>
                    <p>Have new books to add to your collection? You can do that easily here.</p>
                    <a href="book-form.jsp" class="b">Add Book</a>
                </div>
                <div class="box2">
                    <img src="images/reading.jpg" alt="Reading a Book">
                </div>
            </div>
            <div class="wrapper">
                <div class="box1">
                    <img src="images/book.jpg" alt="Reading a Book">
                </div>
                <div class="box2">
                    <h2>Delete a Book Entry</h2>
                    <p>No longer on a book? Delete a book entry here.</p>
                    <a href="delete-book.jsp" class="b">Delete Book</a>
                </div>
            </div>
        </form>
    </main>
</body>
</html>