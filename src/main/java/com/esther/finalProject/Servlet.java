//source = https://www.javaguides.net/2019/03/jsp-servlet-hibernate-crud-example.html

package com.esther.finalProject;

import java.io.*;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;


@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        response.setContentType("text/html");

        if (request.getParameter("view") != null) {
            getAllBooks(request,response);

        } else if (request.getParameter("add") != null) {
            insertBook(request,response);

        } else if (request.getParameter("delete") != null) {
            eraseBook(request,response);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>This is not in use</h1>");
        out.println("</body></html>");
    }

    private void getAllBooks(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        BooksDao bd = BooksDao.getInstance();
        List<myBooks> lb = bd.listBooks();
        request.setAttribute("listBooks", lb);
        RequestDispatcher dispatcher = request.getRequestDispatcher("book-list.jsp");
        dispatcher.forward(request, response);
    }

    private void insertBook(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        BooksDao bd = BooksDao.getInstance();
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        String publisher = request.getParameter("publisher");
        int pages = Integer.parseInt(request.getParameter("pages"));
        String genre = request.getParameter("genre");
        String description = request.getParameter("description");
        bd.addBooks(title, author, publisher, pages, genre, description);
        String message = "You have successfully added your book to your database.";
        request.setAttribute("message", message);
        request.getRequestDispatcher("success.jsp").forward(request, response);
    }

    private void eraseBook(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        BooksDao bd = BooksDao.getInstance();
        int id = Integer.parseInt(request.getParameter("id"));
        bd.deleteBooks(id);
        String message = "You have successfully deleted your book entry from your database.";
        request.setAttribute("message", message);
        request.getRequestDispatcher("success.jsp").forward(request, response);
    }

}