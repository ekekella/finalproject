<%--suppress ALL --%>

<%--
  Created by IntelliJ IDEA.
  User: estherhsia
  Date: 3/29/21
  Time: 11:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <title>My Book List</title>
    <link rel="stylesheet" href="main.css">
</head>

<body>
    <header>
        <div class="container">
            <img src="images/mainPic.jpg" alt="Girl Reading in Library" style="width:100%;">
            <div class="topLeft"><h3 class="hT">Esther's Database</h3></div>
            <div class="centered">
                <p class="hT">Organization</p>
                <h1 class="hT">Begins Here With You</h1>
            </div>
        </div>
    </header>

    <main>
        <h1>List of Your Books</h1>
        <section class="container">
            <c:forEach var="myBooks" items="${listBooks}">
                <div class="searchBox">
                    <p>Book ID: <c:out value="${myBooks.id}"></c:out></p>
                    <p>Title: <c:out value="${myBooks.title}"></c:out></p>
                    <p>Author: <c:out value="${myBooks.author}"></c:out></p>
                    <p>Publisher: <c:out value="${myBooks.publisher}"></c:out></p>
                    <p>Number of Pages: <c:out value="${myBooksooks.pages}"></c:out></p>
                    <p>Genre: <c:out value="${myBooksooks.genre}"></c:out></p>
                    <p>Description of Book: <c:out value="${myBooks.description}"></c:out></p>
                </div>
            </c:forEach>
        </section>

        <form action="Servlet" method="post">
            <div class="wrapper">
                <div class="box1">
                    <h2>Delete a Book Entry</h2>
                    <p>No longer on a book? Delete a book entry here.</p>
                    <a href="delete-book.jsp" class="b">Delete Book</a>
                </div>
                <div class="box2">
                    <img src="images/book.jpg" alt="Reading a Book">
                </div>
            </div>
            <div class="wrapper">
                <div class="box1" id="word6">
                    <img src="images/reading.jpg" alt="Reading a Book">
                </div>
                <div class="box2">
                    <h2>Add to My Library</h2>
                    <p>Have new books to add to your collection? You can do that easily here.</p>
                    <a href="book-form.jsp" class="b">Add Book</a>
                </div>
            </div>
            <div class="intro">
                <div class="begin">
                    <p>Click below for Home Page</p>
                    <a href="index.jsp"><img src="images/home.png" alt="Home Link" width="100px"></a>
                </div>
            </div>
        </form>
    </main>
</body>
</html>

