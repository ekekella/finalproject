<%--
  Created by IntelliJ IDEA.
  User: estherhsia
  Date: 4/6/21
  Time: 3:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <title>Book Management - Delete Book</title>
    <link rel="stylesheet" href="main.css">
</head>

<body>
<header>
    <div class="container">
        <img src="images/mainPic.jpg" alt="Girl Reading in Library" style="width:100%;">
        <div class="topLeft"><h3 class="hT">Esther's Database</h3></div>
        <div class="centered">
            <p class="hT">Organization</p>
            <h1 class="hT">Begins Here With You</h1>
        </div>
    </div>
</header>
<main>
    <div class="intro">
        <div class="begin">
            <h1>Delete a Book Entry</h1>
            <p>Fill in ID number and title of book to delete entry from database. ** If you're not sure what the ID number is, click the link below to view your books. **</p>
        </div>
    </div>
    <div>
        <form action="Servlet" method="post">
            <div class="wrapper">
                <div class="box1">
                    <input type="submit" name="view" value="View Books" class="bSub">
                </div>
            </div>
        </form>
        <form action="Servlet" method="post">
            <fieldset class="addTable">
                <label class="top" id="dId">ID Number: *<input type="number" name="id" pattern="^\d*[1-9]\d*$" required></label>
            </fieldset>
            <div>
                <input type="submit" name="delete" value="Delete Book" class="bfb">
            </div>
        </form>
    </div>
    <div class="intro">
        <div class="begin">
            <p>Click below for Home Page</p>
            <a href="index.jsp"><img src="images/home.png" alt="Home Link" width="100px"></a>
        </div>
    </div>
</main>
</body>
</html>
