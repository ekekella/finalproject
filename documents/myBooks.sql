-- MySQL dump 10.13  Distrib 8.0.22, for macos10.15 (x86_64)
--
-- Host: localhost    Database: booksProject
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `myBooks`
--

DROP TABLE IF EXISTS `myBooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `myBooks` (
  `myBooksID` int NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `author` varchar(45) NOT NULL,
  `publisher` varchar(45) NOT NULL,
  `pages` int NOT NULL,
  `genre` varchar(45) NOT NULL,
  `description` varchar(1000) NOT NULL,
  PRIMARY KEY (`myBooksID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `myBooks`
--

LOCK TABLES `myBooks` WRITE;
/*!40000 ALTER TABLE `myBooks` DISABLE KEYS */;
INSERT INTO `myBooks` VALUES (2,'Magic Triumphs','Ilona Andrews','Ace',327,'Urban Fantasy','Kate has come a long way from her origins as a loner taking care of paranormal problems in post-Shift Atlanta. She\'s made friends and enemies. She\'s found love and started a family with Curran Lennart, the former Beast Lord. But her magic is too strong for the power players of the world to let her be.');
INSERT INTO `myBooks` VALUES (3,'American Demon','Kim Harrison','Ace',473,'Urban Fantasy','What happens after you\'ve saved the world? Well, if you\'re Rachel Mariana Morgan, witch-born demon, you quickly discover that something might have gone just a little bit wrong. That the very same acts you and your friends took to forge new powers may have released something bound by the old. With a rash of zombies, some strange new murders, and an exceedingly mysterious new demon in town, it will take everything Rachel has to counter this new threat to the world--and it may demand the sacrifice of what she holds most dear. ');
INSERT INTO `myBooks` VALUES (4,'Something Borrowed','Emily Griffin','St. Martin\'s Press',323,'Fiction','Rachel has always been the consummate good girl—until her thirtieth birthday, when her best friend, Darcy, throws her a party. That night, after too many drinks, Rachel ends up in bed with Darcy\'s fiancé. Although she wakes up determined to put the one-night fling behind her, Rachel is horrified to discover that she has genuine feelings for the one guy she should run from. As the September wedding date nears, Rachel knows she has to make a choice. In doing so, she discovers that the lines between right and wrong can be blurry, endings aren\'t always neat, and sometimes you have to risk all to win true happiness.');
INSERT INTO `myBooks` VALUES (5,'The Da Vinci Code','Dan Brown','Anchor',489,'Fiction','While in Paris, Harvard symbologist Robert Langdon is awakened by a phone call in the dead of the night. The elderly curator of the Louvre has been murdered inside the museum, his body covered in baffling symbols. As Langdon and gifted French cryptologist Sophie Neveu sort through the bizarre riddles, they are stunned to discover a trail of clues hidden in the works of Leonardo da Vinci—clues visible for all to see and yet ingeniously disguised by the painter.');
INSERT INTO `myBooks` VALUES (6,'Peace Talks','Jim Butcher','Ace',340,'Urban Fantasy','When the Supernatural nations of the world meet up to negotiate an end to ongoing hostilities, Harry Dresden, Chicago\'s only professional wizard, joins the White Council\'s security team to make sure the talks stay civil. But can he succeed, when dark political manipulations threaten the very existence of Chicago--and all he holds dear?');
INSERT INTO `myBooks` VALUES (7,'All The Light We Cannot See','Anthony Doerr','Scribner',531,'Fiction','From the highly acclaimed, multiple award-winning Anthony Doerr, the stunningly beautiful instant New York Times bestseller about a blind French girl and a German boy whose paths collide in occupied France as both try to survive the devastation of World War II.');
INSERT INTO `myBooks` VALUES (8,'The Nightingale','Kristin Hannah','St. Martin\'s Press',440,'Fiction','In the quiet village of Carriveau, Vianne Mauriac says good-bye to her husband, Antoine, as he heads for the Front. She doesn’t believe that the Nazis will invade France…but invade they do, in droves of marching soldiers, in caravans of trucks and tanks, in planes that fill the skies and drop bombs upon the innocent. When a German captain requisitions Vianne’s home, she and her daughter must live with the enemy or lose everything. Without food or money or hope, as danger escalates all around them, she is forced to make one impossible choice after another to keep her family alive.');
INSERT INTO `myBooks` VALUES (9,'Becoming','Michelle Obama','Crown',426,'Autobiography','In her memoir, a work of deep reflection and mesmerizing storytelling, Michelle Obama invites readers into her world, chronicling the experiences that have shaped herâfrom her childhood on the South Side of Chicago to her years as an executive balancing the demands of motherhood and work, to her time spent at the worldâs most famous address. With unerring honesty and lively wit, she describes her triumphs and her disappointments, both public and private, telling her full story as she has lived itâin her own words and on her own terms. Warm, wise, and revelatory, Becoming is the deeply personal reckoning of a woman of soul and substance who has steadily defied expectationsâand whose story inspires us to do the same');
/*!40000 ALTER TABLE `myBooks` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-06 23:12:02
