<%--
  Created by IntelliJ IDEA.
  User: estherhsia
  Date: 3/29/21
  Time: 11:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Book Management - Add Book</title>
    <link rel="stylesheet" href="main.css">
</head>

<body>
    <header>
        <div class="container">
            <img src="images/mainPic.jpg" alt="Girl Reading in Library" style="width:100%;">
            <div class="topLeft"><h3 class="hT">Esther's Database</h3></div>
            <div class="centered">
                <p class="hT">Organization</p>
                <h1 class="hT">Begins Here With You</h1>
            </div>
        </div>
    </header>

    <main>
        <div class="intro">
            <div class="begin">
                <h1>Book Management</h1>
                <p>Fill in ALL fields below to add your book to your database.</p>
            </div>
        </div>
        <div>
            <form action="Servlet" method="post">
                <fieldset class="addTable">
                    <label class="top" id="bTitle">Title: * <input type="text" name="title" pattern="[a-zA-Z -._]{5,}" required></label>
                    <label class="top" id="bAuthor">Author: * <input type="text" name="author" pattern="[a-zA-Z -._]{5,}" required></label>
                    <label class="top" id="bPublisher">Publisher: * <input type="text" name="publisher"></label>
                    <label class="top" id="bPages">Number of Pages: * <input type="number" name="pages" max="99999" required></label>
                    <label class="top" id="bGenre">Genre: * <input type="text" name="genre" pattern="[a-zA-Z -._]{5,}"></label>
                    <label class="top" id="bDescription">Description of Book: * <textarea rows="10" cols="40" name="description" required></textarea></label>
                </fieldset>
                <div>
                    <input type="submit" name="add" value="Add Book" class="bfb">
                </div>
            </form>
        </div>
        <div>
            <form action="Servlet" method="post">
            <div class="wrapper">
                <div class="box1" id="word4">
                    <h2>My Library of Books</h2>
                    <p>Sift through your collection of books here.</p>
                    <input type="submit" name="view" value="View Books" class="bSub">
                </div>
                <div class="box2">
                    <img src="images/sorting.jpg" alt="Library">
                </div>
            </div>
                <div class="intro">
                    <div class="begin">
                        <p>Click below for Home Page</p>
                        <a href="index.jsp"><img src="images/home.png" alt="Home Link" width="100px"></a>
                    </div>
                </div>
            </form>
        </div>
    </main>
    </body>
</html>
