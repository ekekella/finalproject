package com.esther.finalProject;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class BooksDao {
    SessionFactory factory;
    Session session;

    private static BooksDao single_instance = null;

    private BooksDao() {
        factory = HibernateUtils.getSessionFactory();
    }

    public static BooksDao getInstance() {
        if (single_instance == null) {
            single_instance = new BooksDao();
        }
        return single_instance;
    }

    public List<myBooks> listBooks() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.esther.finalProject.myBooks";
            List<myBooks> lb = (List<myBooks>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return lb;
        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public void addBooks(String title, String author, String publisher, int pages, String genre, String description) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            myBooks books = new myBooks(title, author, publisher, pages, genre, description);
            session.save(books);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    public void deleteBooks(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            myBooks books = session.get(myBooks.class, id);
            session.delete(books);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }
}
