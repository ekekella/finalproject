
<%--
  Created by IntelliJ IDEA.
  User: estherhsia
  Date: 3/30/21
  Time: 9:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Success</title>
    <link rel="stylesheet" href="main.css">
</head>

<body>
<header>
    <div class="container">
        <img src="images/mainPic.jpg" alt="Girl Reading in Library" style="width:100%;">
        <div class="topLeft"><h3 class="hT">Esther's Database</h3></div>
        <div class="centered">
            <p class="hT">Organization</p>
            <h1 class="hT">Begins Here With You</h1>
        </div>
    </div>
</header>

<main>
    <div class="intro">
        <div class="begin">
            <h2>${message}</h2>
            <p>Click below for Home Page</p>
            <a href="index.jsp"><img src="images/home.png" alt="Home Link" width="100px"></a>
        </div>
    </div>
</main>

</body>
</html>
